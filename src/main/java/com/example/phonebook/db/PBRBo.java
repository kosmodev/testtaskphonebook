package com.example.phonebook.db;

import com.example.phonebook.model.PhoneBookRow;

import java.util.List;

public interface PBRBo {
    void save(PhoneBookRow row);
    void update(PhoneBookRow row);
    void delete(PhoneBookRow row);
    List<PhoneBookRow> allItems();
}
