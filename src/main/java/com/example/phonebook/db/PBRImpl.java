package com.example.phonebook.db;

import com.example.phonebook.model.PhoneBookRow;
import org.hibernate.Session;
import org.hibernate.query.Query;

import java.util.ArrayList;
import java.util.List;

public class PBRImpl implements PBRBo{

    @Override
    public void save(PhoneBookRow row) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        session.save(row);
        session.getTransaction().commit();

    }

    @Override
    public void update(PhoneBookRow row) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        session.update(row);
        session.getTransaction().commit();
        session.close();
    }

    @Override
    public void delete(PhoneBookRow row) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        session.remove(row);
        session.getTransaction().commit();
        session.close();
    }

    @Override
    public List<PhoneBookRow> allItems() {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Query q = session.createQuery("From PhoneBookRow ");
        List<PhoneBookRow> phoneBookRows = q.list();
        if (phoneBookRows == null) {
            phoneBookRows = new ArrayList<>();
        }
        session.close();
        return phoneBookRows;
    }
}
