package com.example.phonebook.jsf;

import com.example.phonebook.db.PBRImpl;
import com.example.phonebook.jsf.dialogsUtlis.PBDialogNewBean;
import com.example.phonebook.model.PhoneBookRow;
import org.primefaces.context.RequestContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.ManagedBean;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import java.io.Serializable;
import java.util.List;

@Component
@ManagedBean
@SessionScoped
public class PhoneBookBean implements Serializable {

    @Autowired
    private PhoneBookDao mPhoneBookDao;
    private PhoneBookRow mEditedModel;

    private PBRImpl mPbrDB = new PBRImpl();

    public List<PhoneBookRow> fetchPhoneBookDetails() {
        return mPhoneBookDao.getPBDetails();
    }

    public void showEditeItemDialog(PhoneBookRow bookRow) {
        setEditedModel(bookRow);
        commandDialog("PF('dlgUpdate').show();");
    }

    private void commandDialog(String s) {
        try{
            RequestContext context = RequestContext.getCurrentInstance();
            context.execute(s);
        }catch (Exception ignored){
        }
    }

    public void showNewItemDialog() {
        setEditedModel(new PhoneBookRow());
        commandDialog("PF('dlgNew').show();");
    }

    public void remove(PhoneBookRow bookRow) {
        mPbrDB.delete(bookRow);
        updateTable();
    }

    public void update() {
        PhoneBookRow phoneBookRow = new PhoneBookRow(
                mEditedModel.getFirstName(),
                mEditedModel.getLastName(),
                mEditedModel.getPhone());
        phoneBookRow.setId(mEditedModel.getId());

        mPbrDB.update(phoneBookRow);

        updateTable();
        commandDialog("PF('dlgUpdate').hide();");
    }

    public void add(PBDialogNewBean value) {
        mPbrDB.save(value.getPhoneBookRow());

        value.clear();
        updateTable();
        commandDialog("PF('dlgNew').hide();");
    }

    private void updateTable() {
        try {
            fetchPhoneBookDetails();
            FacesContext ctxt = FacesContext.getCurrentInstance();
            ctxt.getPartialViewContext().getRenderIds().add("table");
        } catch (Exception ignored) {

        }
    }

    public void setEditedModel(PhoneBookRow mEditedModel) {
        this.mEditedModel = mEditedModel;
    }

    public PhoneBookRow getEditedModel() {
        return mEditedModel;
    }

    public void setPhoneBookDao(PhoneBookDao value) {
        this.mPhoneBookDao = value;
    }
}
