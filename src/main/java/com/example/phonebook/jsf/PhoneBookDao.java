package com.example.phonebook.jsf;

import com.example.phonebook.model.PhoneBookRow;

import java.util.List;

public interface PhoneBookDao {
    List<PhoneBookRow> getPBDetails();
}
