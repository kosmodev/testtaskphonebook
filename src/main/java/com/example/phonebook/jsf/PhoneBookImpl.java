package com.example.phonebook.jsf;

import com.example.phonebook.db.PBRImpl;
import com.example.phonebook.model.PhoneBookRow;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PhoneBookImpl implements PhoneBookDao {

    @Override
    public List<PhoneBookRow> getPBDetails() {
        PBRImpl pbrDB= new PBRImpl();
        return pbrDB.allItems();
    }
}
