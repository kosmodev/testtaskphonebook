package com.example.phonebook.jsf.dialogsUtlis;

import com.example.phonebook.jsf.PhoneBookDao;
import com.example.phonebook.model.PhoneBookRow;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.ManagedBean;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;

@Component
@ManagedBean
@SessionScoped
public class PBDialogNewBean implements Serializable{
    @Autowired
    private PhoneBookDao phoneBookDao;
    private String firstName;
    private String lastName;
    private String phone;

    public PhoneBookDao getPhoneBookDao() {
        return phoneBookDao;
    }

    public void setPhoneBookDao(PhoneBookDao phoneBookDao) {
        this.phoneBookDao = phoneBookDao;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public void clear(){
        setFirstName("");
        setLastName("");
        setPhone("");
    }

    public PhoneBookRow getPhoneBookRow(){
        return new PhoneBookRow(
                getFirstName(),
                getLastName(),
                getPhone());
    }
}
