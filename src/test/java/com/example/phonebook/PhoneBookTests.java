package com.example.phonebook;

import com.example.phonebook.jsf.PhoneBookBean;
import com.example.phonebook.jsf.PhoneBookImpl;
import com.example.phonebook.jsf.dialogsUtlis.PBDialogNewBean;
import com.example.phonebook.model.PhoneBookRow;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(SpringJUnit4ClassRunner.class)
public class PhoneBookTests {
    private static PhoneBookBean sPhoneBookBean;
    private static PhoneBookRow sPhoneBookRow;

    @BeforeClass
    public static void initialise() {
        sPhoneBookBean = new PhoneBookBean();
        sPhoneBookBean.setPhoneBookDao(new PhoneBookImpl());
    }

    @Test
    public void testAddNewItem() {
        PBDialogNewBean pbDialogNewBean = new PBDialogNewBean();
        pbDialogNewBean.setPhone("034");
        pbDialogNewBean.setFirstName("Vlad");
        pbDialogNewBean.setLastName("Vlad");
        sPhoneBookBean.add(pbDialogNewBean);

        List<PhoneBookRow> phoneBookRows = sPhoneBookBean.fetchPhoneBookDetails();
        int size = phoneBookRows.size();
        assertEquals(1, size);

        sPhoneBookRow = phoneBookRows.get(0);
    }


    @Test
    public void testUpdateItem() {
        sPhoneBookRow.setFirstName("Vlad");
        sPhoneBookRow.setLastName("Vlad");
        sPhoneBookRow.setPhone("034");

        sPhoneBookBean.showEditeItemDialog(sPhoneBookRow);
        sPhoneBookBean.update();

        List<PhoneBookRow> phoneBookRows = sPhoneBookBean.fetchPhoneBookDetails();
        int size = phoneBookRows.size();
        assertEquals(1, size);

        assertEquals(phoneBookRows.get(0).getFirstName(), "Vlad");
        assertEquals(phoneBookRows.get(0).getLastName(), "Vlad");
        assertEquals(phoneBookRows.get(0).getPhone(), "034");
    }

    @Test
    public void testRemoveItem() {
        sPhoneBookRow.setFirstName("Vlad");
        sPhoneBookRow.setLastName("Vlad");
        sPhoneBookRow.setPhone("034");

        sPhoneBookBean.remove(sPhoneBookRow);

        List<PhoneBookRow> phoneBookRows = sPhoneBookBean.fetchPhoneBookDetails();
        int size = phoneBookRows.size();
        assertEquals(0, size);
    }
}
